resource "aws_iam_role" "lambda-role" {
  name = "ec2-dns-maintainer"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = {
    Name              = "ec2-dns-maintainer"
    Service           = "${var.service_name}"
    Environment       = "${var.environment}"
    ResponsibleParty  = "${var.responsible_party}"
    ResponsibleParty2 = "${var.responsible_party2}"
    DataRisk          = "${var.data_risk}"
    ComplianceRisk    = "${var.compliance_risk}"
    Documentation     = "${var.documentation}"
    Comments          = "Maintain DNS for EC2 instances"
    VCS               = "${var.vcs}"
  }
}

resource "aws_iam_role_policy" "policy" {
  name = "ec2-dns-maintainer"
  role = "${aws_iam_role.lambda-role.name}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "route53:GetHostedZone",
        "route53:ChangeResourceRecordSets"
      ],
      "Resource": [
        "arn:aws:route53:::hostedzone/*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:DescribeLogStreams",
        "logs:PutLogEvents"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "ec2:DescribeInstances",
        "ec2:DescribeTags"
      ],
      "Resource": ["${join("\", \"", var.permitted_zones)}"]
    }
  ]
}
EOF
}

data "archive_file" "function" {
  type        = "zip"
  output_path = "/tmp/function.zip"
  source_dir  = "${path.module}/lambda/ec2_dns_maintainer"
}

resource "aws_lambda_function" "function" {
  description      = "Updates DNS for EC2 instances based on Tag values"
  filename         = "${data.archive_file.function.output_path}"
  function_name    = "ec2-dns-maintainer"
  role             = "${aws_iam_role.lambda-role.arn}"
  handler          = "ec2-dns-maintainer.handler"
  runtime          = "python3.6"
  timeout          = 60
  source_code_hash = filebase64sha256(data.archive_file.function.output_path)

  tags = {
    Name              = "ec2-dns-maintainer"
    Service           = "${var.service_name}"
    Environment       = "${var.environment}"
    ResponsibleParty  = "${var.responsible_party}"
    ResponsibleParty2 = "${var.responsible_party2}"
    DataRisk          = "${var.data_risk}"
    ComplianceRisk    = "${var.compliance_risk}"
    Documentation     = "${var.documentation}"
    Comments          = "Updates DNS for EC2 instances based on Tag values"
    VCS               = "${var.vcs}"
  }
}

#####
## Setup an event rule to trigger our Lambda function
#####
resource "aws_cloudwatch_event_rule" "event" {
  name        = "capture-ec2-startup"
  description = "Detect when a new EC2 instance starts up update DNS using lambda function"

  event_pattern = <<PATTERN
{
  "source": [
    "aws.ec2"
  ],
  "detail-type": [
    "EC2 Instance State-change Notification"
  ],
  "detail": {
    "state": [
      "running"
    ]
  }
}
PATTERN
}

resource "aws_cloudwatch_event_target" "lambda_target" {
  rule      = "${aws_cloudwatch_event_rule.event.name}"
  target_id = "ec2-dns-maintainer"
  arn       = "${aws_lambda_function.function.arn}"

  input_transformer {
    input_paths = { "instance" : "$.detail.instance-id" }

    input_template = <<TEMPLATE
{
    "instance": <instance>
}
TEMPLATE
  }
}

resource "aws_lambda_permission" "allow_cloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.function.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.event.arn
}

